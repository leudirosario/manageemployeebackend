﻿using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Infraestructure.Data;
using Infraestructure.Contracts;
using Infraestructure.Data.Repositories;

namespace V8EmployeeManager
{
    public class RepositoryTest
    {

       
        [Test]
        public void TestReturnAllRegister()
        {
            //var context = new Mock<AppDbContext>();
            var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "EmployeeDataBase")
            .Options;

            var salary1 = new EmployeeSalary()
            {
                BaseSalary = 300,
                Compensation = 0,
            };
            var salary2 = new EmployeeSalary()
            {
                BaseSalary = 0,
                Compensation = 0,
            };
            List<EmployeeSalary> salaries = new List<EmployeeSalary> { salary1, salary2 };
            using (var context = new AppDbContext(options))
            {
                context.Salaries.AddRange(salaries);
                context.SaveChanges();
                int total_register = context.Salaries.Count();
                var employeeRepository = new EmployeeSalaryRespository(context);
                List<EmployeeSalary> dbSalaries = null;
                Task.Run(async () =>
                {
                    dbSalaries = (await employeeRepository.GetAll(false)).ToList();

                }).Wait();
                
                Assert.AreEqual(salaries, dbSalaries);
            }

        }
        [Test]
        public void TestReturnOneRegister()
        {
            //var context = new Mock<AppDbContext>();
            var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "EmployeeDataBase")
            .Options;
            var salary1 = new EmployeeSalary()
            {
                EmployeeId = 90,
                BaseSalary = 300,
                Compensation = 0,
            };
            using (var context = new AppDbContext(options))
            {
                context.Salaries.Add(salary1);
                context.SaveChanges();

                var employeeRepository = new EmployeeSalaryRespository(context);
                EmployeeSalary dbSalary = null;
                Task.Run(async () =>
                {
                    dbSalary = await employeeRepository.FindOne(c => c.EmployeeId == 90, false);

                }).Wait();
                Assert.AreEqual(dbSalary.BaseSalary, salary1.BaseSalary);
                context.Remove(dbSalary);
                context.SaveChanges();
            }

        }
        [Test]
        public void Delete()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
           .UseInMemoryDatabase(databaseName: "EmployeeDataBase")
           .Options;
            var salary1 = new EmployeeSalary()
            {
                EmployeeId = 90,
                BaseSalary = 300,
                Compensation = 0,
            };
            using (var context = new AppDbContext(options))
            {
                context.Salaries.Add(salary1);
                context.SaveChanges();

                var employeeRepository = new EmployeeSalaryRespository(context);
                EmployeeSalary dbSalary = null;
                bool isDelete = false;
                Task.Run(async () =>
                {
                    dbSalary = await employeeRepository.FindOne(c => c.EmployeeId == 90, false);
                    isDelete = await employeeRepository.Delete(dbSalary.Id,false);

                }).Wait();
                Assert.AreEqual(true, isDelete);
            }
        }
    }
}
