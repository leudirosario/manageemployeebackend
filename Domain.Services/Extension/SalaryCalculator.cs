﻿using Domain.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Extension
{
    public static class SalaryCalculator
    {
        public static void ApplyTotalSalaryAmount(this EmployeeSalaryDto salary)
        {
            salary.OtherIncome = (salary.BaseSalary + salary.Commission) * 0.8m + salary.Commission;
            salary.TotalSalary = salary.BaseSalary
                + salary.ProductionBonus
                + (salary.Compensation * 0.75m)
                + salary.OtherIncome
                + salary.Constributions;
        }
    }
}
