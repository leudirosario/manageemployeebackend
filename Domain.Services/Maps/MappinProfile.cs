﻿using AutoMapper;
using Domain.Entities;
using Domain.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Maps
{
    public class MappinProfile : Profile
    {
        public MappinProfile()
        {
            CreateMap<EmployeeSalary, EmployeeSalaryDto>()
                .ReverseMap();

            CreateMap<EmployeeSalary, EmployeeSalaryDto>()
                .ForMember(c => c.Division, opt => opt.MapFrom(d => d.Division.Name))
                .ForMember(c => c.Office, opt => opt.MapFrom(d => d.Office.Name))
                .ForMember(c => c.Position, opt => opt.MapFrom(d => d.Position.Name))
                .ForMember(c => c.Id, opt => opt.MapFrom(d => d.Id))
                .ForMember(c => c.BirthDay, opt => opt.MapFrom(d => d.Employee.BirthDay))
                .ForMember(c => c.IdentificacionNumber, opt => opt.MapFrom(d => d.Employee.IdentificacionNumber));


            CreateMap<Office, OfficeDto>().ReverseMap();
            CreateMap<Position, PositionDto>().ReverseMap();
            CreateMap<Division, DivisionDto>().ReverseMap();
            CreateMap<Employee, EmployeeDto>().ReverseMap();
            CreateMap<Employee, EmployeeDto>()
                .ForMember(sc => sc.EmployeeCode, dest => dest.MapFrom(c => c.Code))
                .ForMember(sc => sc.EmployeeName, dest => dest.MapFrom(c => c.Name + " " + c.SurName))
                .ForMember(sc => sc.EmployeeSurName, dest => dest.MapFrom(c => c.SurName));



        }
    }
}
