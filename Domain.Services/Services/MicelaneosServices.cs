﻿using AutoMapper;
using Domain.Shared.DTOs;
using Infraestructure.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Services
{
    public class MicelaneosServices : IMicelaneosServices
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public MicelaneosServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public async Task<List<OfficeDto>> GetAllOffices()
        {
            var offices = await unitOfWork.OfficeRepository.GetAll();
            var offficesdtos = mapper.Map<List<OfficeDto>>(offices);
            return offficesdtos;
        }

        public async Task<List<PositionDto>> GetAllPosition()
        {
            var positions = await unitOfWork.PositionRepository.GetAll();
            var positionsDto = mapper.Map<List<PositionDto>>(positions);
            return positionsDto;
        }
        public async Task<List<DivisionDto>> GetAllDivision()
        {
            var divisions = await unitOfWork.DivisionRepository.GetAll();
            var divisionsdto = mapper.Map<List<DivisionDto>>(divisions);
            return divisionsdto;
        }





    }
}
