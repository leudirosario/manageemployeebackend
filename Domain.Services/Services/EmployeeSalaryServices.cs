﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Services.Extension;
using Domain.Services.Interfaces;
using Domain.Shared.Criterials;
using Domain.Shared.DTOs;
using Infraestructure.Contracts;
using Infraestructure.Data;
using Infraestructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Services
{
    public class EmployeeSalaryServices : IEmployeeSalaryService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork UnitOfWork;
        public EmployeeSalaryServices(IMapper mapper, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.UnitOfWork = unitOfWork;
        }
        private void ValidateSalary(List<EmployeeSalaryDto> Salaries)
        {
            foreach (var Salary in Salaries)
            {
                var VerificacionPeriodo = UnitOfWork.EmployeeSalaryRepository
                    .FindOne(empSalary =>
                    empSalary.EmployeeId == Salary.EmployeeID
                    && empSalary.Year == Salary.Year
                    && empSalary.Month == Salary.Month);

                if (VerificacionPeriodo != null)
                    throw new ArgumentException($"No se puede generar el periodo (mes {Salary.Month} / año {Salary.Year} ) de salario para este empleado, ya existe un registro creado para este periodo");
            }
        }
        public async Task AddSalaries(List<EmployeeSalaryDto> Salaries)
        {
            try
            {
                // Requerimiento 2-Validacion de salario de mes y añó debe ser unico
                // if something fail in validation salary will be a throw error
                 ValidateSalary(Salaries);
                Salaries.ForEach(salary =>
                {
                    var _Salary = mapper.Map<EmployeeSalary>(salary);
                    UnitOfWork.EmployeeSalaryRepository.Add(_Salary);
                });
                await UnitOfWork.SaveChangeAsync();
            }
            catch
            {
                throw;
            }

        }

        public async Task<IEnumerable<EmployeeSalaryDto>> GetAllSalaries(SalaryCriteria salaryCriteria)
        {
            var SalarieQueryrable = (await UnitOfWork.EmployeeSalaryRepository.GetAll(c => c.Id > 0));
            if (salaryCriteria.OfficeId > 0)
            {
                SalarieQueryrable = SalarieQueryrable.Where(c => c.OfficeId == salaryCriteria.OfficeId);
            }
            if (salaryCriteria.Grade > 0)
            {
                SalarieQueryrable = SalarieQueryrable.Where(c => c.Grade == salaryCriteria.Grade);
            }
            if (salaryCriteria.PositionId > 0)
            {
                SalarieQueryrable = SalarieQueryrable.Where(c => c.PositionId == salaryCriteria.PositionId);
            }
            var salaries = mapper.Map<List<EmployeeSalaryDto>>(SalarieQueryrable.ToList());
            // calculate all total to pay
            salaries.ForEach(s => s.ApplyTotalSalaryAmount());
            return salaries;
        }
    }
}
