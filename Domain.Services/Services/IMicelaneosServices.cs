﻿using Domain.Shared.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Services.Services
{
    public interface IMicelaneosServices
    {
        Task<List<DivisionDto>> GetAllDivision();
        Task<List<OfficeDto>> GetAllOffices();
        Task<List<PositionDto>> GetAllPosition();
    }
}