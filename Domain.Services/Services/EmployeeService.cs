﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Shared.Criterials;
using Domain.Shared.DTOs;
using Infraestructure.Contracts;
using Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork UnitOfWork;
        public EmployeeService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            UnitOfWork = unitOfWork;
        }

        public async Task<int> AddNewEmployee(EmployeeDto employeeDto)
        {
            await ValidateNameAndCode(employeeDto);
            var employee = mapper.Map<Employee>(employeeDto);
            await UnitOfWork.EmployeeRepository.Add(employee);

            return 0;

        }

        private async Task ValidateNameAndCode(EmployeeDto employeeDto)
        {
            // valida que no exista el mismo codigo
            var findInDb = await UnitOfWork.EmployeeRepository.FindOne(c => c.Code == employeeDto.EmployeeCode);
            if (findInDb != null)
                throw new ArgumentException("Ya existe un registro con este codigo");

            // valida que no exista el nombre completo
            var findInDbByFullName = await UnitOfWork.EmployeeRepository
                .FindOne(c =>
                c.Name == employeeDto.EmployeeName &&
                c.SurName == employeeDto.EmployeeSurName
                );
            if (findInDb != null)
                throw new ArgumentException("Ya existe un registro con este nombre");
        }
        public async Task<IEnumerable<EmployeeDto>> GetEmployees(EmplyeeCriteria criteria)
        {
            var employees = (await UnitOfWork.EmployeeRepository.GetAll()).ToList();
            return mapper.Map<List<EmployeeDto>>(employees); ;
        }




    }
}
