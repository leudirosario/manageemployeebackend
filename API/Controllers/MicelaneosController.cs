﻿using Domain.Services.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MicelaneosController : ControllerBase
    {
        private readonly IMicelaneosServices services;

        public MicelaneosController(IMicelaneosServices services)
        {
            this.services = services;
        }
        [Route("Offices")]
        [HttpGet]
        public async Task<IActionResult> GetOffices()
        => Ok(await this.services.GetAllOffices());

        [Route("Positions")]
        [HttpGet]
        public async Task<IActionResult> GetPositions()
        => Ok(await this.services.GetAllPosition());

        [Route("Divisions")]
        [HttpGet]
        public async Task<IActionResult> GetDivisions() => Ok(await this.services.GetAllDivision());

    }
}
