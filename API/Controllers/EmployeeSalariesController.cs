﻿using Domain.Interfaces;
using Domain.Services.Interfaces;
using Domain.Shared.Criterials;
using Domain.Shared.DTOs;
using Infraestructure.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeSalariesController : ControllerBase
    {
        private readonly IEmployeeSalaryService employeeSalaryService;
        private readonly IEmployeeService employeeService;

        public EmployeeSalariesController(IEmployeeSalaryService employeeSalaryService, IEmployeeService employeeService)
        {
            this.employeeSalaryService = employeeSalaryService;
            this.employeeService = employeeService;
        }

        [Route("AddNewEmployeeSalary")]
        [HttpPost]
        public async Task<IActionResult> AddNewEmployeeSalary([FromBody] List<EmployeeSalaryDto> employeeSalaries)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await this.employeeSalaryService.AddSalaries(employeeSalaries);
                    return Ok("Salarios agregados");
                }
                catch (System.Exception e)
                {

                }
            }
            return Ok();
        }

        [Route("GetAllEmployeeSalaries")]
        [HttpGet]
        public async Task<IActionResult> GetAllEmployee([FromQuery]SalaryCriteria Criterial)
        {
            try
            {
                var data = await this.employeeSalaryService.GetAllSalaries(Criterial);
                return Ok(data);
            }
            catch (System.Exception)
            {

                throw;
            }

        }
        [Route("GetEmployees")]
        [HttpGet]
        public async Task<IActionResult> GetEmployee()
        {
            return Ok(await this.employeeService.GetEmployees(new EmplyeeCriteria { }));
        }




    }
}
