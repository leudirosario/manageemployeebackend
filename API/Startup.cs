using AutoMapper;
using Domain.Interfaces;
using Domain.Services.Interfaces;
using Domain.Services.Maps;
using Domain.Services.Services;
using FluentValidation.AspNetCore;
using Infraestructure.Contracts;
using Infraestructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = "Server=LEUDIW11;DataBase=v8Employee;Trusted_Connection=True";


            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(connection);
            });
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IEmployeeSalaryService, EmployeeSalaryServices>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IMicelaneosServices, MicelaneosServices>();


            #region mapper config

            var mapperconfig = new AutoMapper.MapperConfiguration(conf =>
            {
                conf.AddProfile(new MappinProfile());
            });
            IMapper mapper = mapperconfig.CreateMapper();
            #endregion
            services.AddSingleton(mapper);
            services.AddControllers().AddFluentValidation();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "V8 Employee Manage", Version = "v1" });
            });
            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options =>
                options.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        );

                c.AddDefaultPolicy(builder =>
                {
                    builder.WithOrigins("http://localhost:4200/")
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });


            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(options => options.AllowAnyOrigin());

            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V8 Employee Manager");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
