﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Employee : EntityBase
    {
        public DateTime BirthDay { get; set; }
        public string Code { get; set; }
        public string SurName { get; set; }
        public string IdentificacionNumber { get; set; }
        public ICollection<EmployeeSalary> Salaries { get; set; }
    }
}
