﻿using System;

namespace Domain.Entities
{
    public class EmployeeSalary
    {
        
        public int Id { get; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int OfficeId { get; set; }
        public Office Office { get; set; }
        // pedding override data
        public int DivisionId { get; set; }
        public Division Division { get; set; }
        public int PositionId    { get; set; }
        public Position Position { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal Compensation { get; set; }
        public decimal Commission { get; set; }
        public decimal Constributions { get; set; }
      



    }
}
