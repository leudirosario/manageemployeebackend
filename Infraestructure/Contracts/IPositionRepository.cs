﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Contracts
{
    public interface IPositionRepository : IGenericRepository<Position>
    {
        
    }
}
