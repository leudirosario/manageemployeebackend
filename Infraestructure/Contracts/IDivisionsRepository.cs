﻿using Domain.Entities;

namespace Infraestructure.Contracts
{
    public interface IDivisionsRepository : IGenericRepository<Division>
    {

    }
}
