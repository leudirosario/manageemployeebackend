﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infraestructure.Contracts
{
    public interface IGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> expression);
        Task<T> FindOne(Expression<Func<T, bool>> expression);
        Task<bool> Add(T entity);
        Task<bool> Update(T entity);
        Task<bool> Delete(int ID);


    }
}
