﻿using Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Contracts
{
    public interface IUnitOfWork
    {

        IEmployeeRepository EmployeeRepository { get; }
        IEmployeeSalaryRepository EmployeeSalaryRepository { get; }
        IDivisionsRepository DivisionRepository { get; }
        IOfficesRepository OfficeRepository { get; }
        IPositionRepository PositionRepository { get;  }
        Task SaveChangeAsync();

    }
}
