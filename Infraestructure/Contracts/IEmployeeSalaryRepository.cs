﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infraestructure.Contracts;

namespace Infraestructure.Contracts
{
    public interface IEmployeeSalaryRepository: IGenericRepository<EmployeeSalary>
    {
        
    }
}
