﻿using Domain.Entities;

namespace Infraestructure.Contracts
{
    public interface IOfficesRepository : IGenericRepository<Office>
    {

    }
}
