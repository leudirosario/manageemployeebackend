﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Extensions
{
    public static class EmployeeSalaryExtentionEF
    {
        public static IQueryable<EmployeeSalary> WithInclude(this IQueryable<EmployeeSalary> salaries)
        {
            salaries = salaries.Include(c => c.Employee)
                .Include(c => c.Position)
                .Include(c => c.Office)
                .Include(c => c.Division);
            return salaries;
        }
    }
}
