﻿using Domain.Entities;
using Domain.Interfaces;
using Domain.Services.Interfaces;
using Infraestructure.Contracts;
using Infraestructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
            EmployeeRepository = new EmployeeRepository(this.context);
            EmployeeSalaryRepository = new EmployeeSalaryRespository(this.context);
            DivisionRepository = new DivisionRepository(this.context);
            OfficeRepository = new OfficeRepository(this.context);
            PositionRepository = new PositionRepository(this.context);
        }


        // inside manage state
        private bool disposed = false;
        private readonly AppDbContext context;

        public IEmployeeRepository EmployeeRepository { get; private set; }

        public IEmployeeSalaryRepository EmployeeSalaryRepository { get; private set; }

        public IDivisionsRepository DivisionRepository { get; private set; }

        public IOfficesRepository OfficeRepository { get; private set; }

        public IPositionRepository PositionRepository { get; private set; }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task SaveChangeAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}
