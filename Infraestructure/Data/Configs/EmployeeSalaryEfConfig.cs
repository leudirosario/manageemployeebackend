﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data.Configs
{
    public class EmployeeSalaryEfConfig : IEntityTypeConfiguration<EmployeeSalary>
    {
        public void Configure(EntityTypeBuilder<EmployeeSalary> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Year).IsRequired();
            builder.Property(c => c.Month).IsRequired();
            
            builder.HasOne<Employee>(c => c.Employee)
                .WithMany(c => c.Salaries)
                .HasForeignKey(c => c.EmployeeId);

            builder.HasOne<Position>(c => c.Position);
            builder.HasOne<Office>(c => c.Office);
            builder.HasOne<Division>(c => c.Division);




        }
    }
}
