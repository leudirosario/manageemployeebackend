﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data.Configs
{
    public class EmployeeEfConfig : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            // setting all validation DB
            builder.Property(c => c.Name).IsRequired();
            builder.Property(c => c.Code).HasMaxLength(10);
            builder.Property(c => c.Name).HasMaxLength(150);
            builder.Property(c => c.SurName).HasMaxLength(150);
            builder.Property(c => c.IdentificacionNumber).HasMaxLength(10);



        }
    }
}
