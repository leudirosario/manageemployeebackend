﻿using Domain.Entities;
using Infraestructure.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Infraestructure.Data.Repositories
{
    public class OfficeRepository : RepositoryBase<Office>, IOfficesRepository
    {
        public OfficeRepository(DbContext dbContext) : base(dbContext)
        {
        }

    }
}
