﻿using Domain.Entities;
using Infraestructure.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data.Repositories
{
    public class DivisionRepository : RepositoryBase<Division>, IDivisionsRepository
    {
        public DivisionRepository(DbContext dbContext) : base(dbContext)
        {
        }

    }
}
