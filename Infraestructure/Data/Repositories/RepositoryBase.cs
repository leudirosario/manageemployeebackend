﻿using Domain.Base;
using Domain.Interfaces;
using Infraestructure.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data.Repositories
{

    public class RepositoryBase<Entity> : IGenericRepository<Entity> where Entity : class
    {
        private DbContext context;
        internal DbSet<Entity> DbSet;
        public RepositoryBase(DbContext dbContext)
        {
            this.context = dbContext;
            this.DbSet = context.Set<Entity>();
        }


        public async Task<bool> Add(Entity entity)
        {
            await DbSet.AddAsync(entity);
            return true;
        }

        public  Task<bool> Delete(int ID)
        {
            throw new NotImplementedException();
        }

        public async Task<Entity> FindOne(Expression<Func<Entity, bool>> expression)
        {
            var dbEntity = await this.DbSet.Where(expression).FirstOrDefaultAsync();
            return dbEntity;
        }

        public async Task<IEnumerable<Entity>> GetAll()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<IEnumerable<Entity>> GetAll(Expression<Func<Entity, bool>> expression)
        {
            return await DbSet.Where(expression).ToListAsync();
        }

        public Task<bool> Update(Entity entity)
        {
            throw new NotImplementedException();
        }
    }
}
