﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Infraestructure.Contracts;
using Microsoft.EntityFrameworkCore;
using Infraestructure.Extensions;

namespace Infraestructure.Data.Repositories
{
    public class EmployeeSalaryRespository : RepositoryBase<EmployeeSalary>, IEmployeeSalaryRepository
    {
        private readonly DbContext dbContext;

        public EmployeeSalaryRespository(DbContext _dbContext) : base(_dbContext)
        {
            this.dbContext = _dbContext;
        }



        public async Task<bool> Add(EmployeeSalary entity)
        {

            await DbSet.AddAsync(entity);
            return true;
        }

        public async Task<bool> Delete(int ID,bool include=true)
        {

            var ToDelete = await FindOne(c => c.Id == ID,include);
            DbSet.Remove(ToDelete);
            return true;


        }

        public async Task<EmployeeSalary> FindOne(Expression<Func<EmployeeSalary, bool>> expression, bool IncludeOtherEntity = true)
        {
            var _salary =
                DbSet.Where(expression).AsQueryable();
            if (IncludeOtherEntity)
            {
                _salary = _salary.WithInclude();
            }
            return await _salary.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<EmployeeSalary>> GetAll(bool IncludeEntities = true)
        {
            // solo para proceso de demo retorno todos los registro
            var salaries  = DbSet.AsQueryable();
                if(IncludeEntities)
            {
                salaries = salaries.WithInclude();
            }
            return await salaries.ToListAsync();
        }

        public async Task<IEnumerable<EmployeeSalary>> GetAll(Expression<Func<EmployeeSalary, bool>> expression)
        {
            return await DbSet.Where(expression).WithInclude().ToListAsync();
        }

        public async Task<bool> Update(EmployeeSalary entity)
        {
            try
            {
                // valido que existe el salario antes de guardar cambios
                var _dbSalary = await FindOne(c => c.Id == entity.Id);
                // solo actualizare los monto de salario, si el cambio es para el cargo, office u otro sera un nuevo registro
                _dbSalary.BaseSalary = entity.BaseSalary;
                _dbSalary.ProductionBonus = entity.ProductionBonus;
                _dbSalary.Compensation = entity.Compensation;
                _dbSalary.Commission = entity.Commission;
                _dbSalary.Constributions = entity.Constributions;
                return true;
            }
            catch (Exception e)
            {

                throw e;
            }

        }
    }
}
