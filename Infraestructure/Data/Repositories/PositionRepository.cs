﻿using Domain.Entities;
using Infraestructure.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Infraestructure.Data.Repositories
{
    public class PositionRepository : RepositoryBase<Position>, IPositionRepository
    {
        public PositionRepository(DbContext dbContext) : base(dbContext)
        {
        }

    }
}
