﻿using Domain.Entities;
using Infraestructure.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        private readonly DbContext dbContext;

        public EmployeeRepository(DbContext dbContext) : base(dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task<Employee> FindOne(Expression<Func<Employee, bool>> expression)
        {
            try
            {

                var employee = await DbSet.Where(expression).FirstOrDefaultAsync();
                return employee;
            }
            catch (Exception e)
            {

                throw new ArgumentException("Empleado no encontrado");
            }


        }

        //TODO: Validar no dupiclidad de nombre
        public async Task<bool> Add(Employee entity)
        {
            await DbSet.AddAsync(entity);
            return true;

        }

        public async Task<bool> Delete(int ID)
        {
            try
            {
                var emplDb = await FindOne(c => c.Id == ID);
                if (emplDb != null)
                {
                    DbSet.Remove(emplDb);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception er)
            {

                throw er;
            }
        }


        public async Task<IEnumerable<Employee>> GetAll()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<IEnumerable<Employee>> GetAll(Expression<Func<Employee, bool>> expression)
        {
            return await DbSet.Where(expression).ToListAsync();
        }

        public async Task<bool> Update(Employee entity)
        {
            var emplDb = await FindOne(c => c.Id == entity.Id);
            if (emplDb != null)
            {
                emplDb.Name = entity.Name;
                emplDb.SurName = entity.SurName;
                emplDb.Code = entity.Code;
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
