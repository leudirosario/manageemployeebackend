﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data
{
    public static class SeeDataExample
    {
        public static void LoadData(AppDbContext context)
        {
            // set all data manipulation for demo propose
            if (!context.Employees.Any())
            {
                // Employee Data
                #region EmployeeData
                var Mike = new Employee
                {
                    Code = "10001222",
                    IdentificacionNumber = "45642132",
                    Name = "MIKE",
                    SurName = "JAMES",
                    BirthDay = new DateTime(1990, 05, 15)
                };
                var Kali = new Employee
                {
                    Code = "10023000",
                    IdentificacionNumber = "14598756",
                    Name = "KALI",
                    SurName = "PRASAD",
                    BirthDay = new DateTime(1980, 1, 25)
                };
                var Jane = new Employee
                {
                    Code = "10023001",
                    IdentificacionNumber = "1989863",
                    Name = "JANE",
                    SurName = "DOE",
                    BirthDay = new DateTime(1978, 11, 18)
                };
                var Ann = new Employee
                {
                    Code = "10001001",
                    IdentificacionNumber = "8563217",
                    Name = "ANN",
                    SurName = "WHITAKER",
                    BirthDay = new DateTime(1992, 06, 21)
                };
                context.Employees.AddRange(new Employee[] { Mike, Kali, Jane, Ann });
                context.SaveChanges();
                #endregion
                #region Positions
                var Cargo_manager = new Position
                {
                    Name = "CARGO MANAGER"
                };
                var HeadofCargo = new Position
                {
                    Name = "HEAD OF CARGO"
                };
                var cargoAssisten = new Position
                {
                    Name = "CARGO ASSISTANT"

                };
                var salesManager = new Position
                {
                    Name = "SALES MANAGER"
                };
                var accountExecutive = new Position
                {
                    Name = "ACCOUNT EXECUTIVE"
                };
                var MarketingAssitan = new Position
                {
                    Name = "MARKETING ASSISTANT"
                };
                var customerDirector = new Position
                {
                    Name = "CUSTOMER DIRECTOR"
                };
                var customerAssitan = new Position
                {
                    Name = "CUSTOMER ASSISTANT"
                };
                context.Positions.AddRange(new Position[] { Cargo_manager, HeadofCargo, cargoAssisten, salesManager, accountExecutive, MarketingAssitan, customerDirector, customerAssitan });
                context.SaveChanges();
                #endregion
                #region  Office
                // office
                var A = new Office { Name = "A" };
                var B = new Office { Name = "B" };
                var C = new Office { Name = "C" };
                var D = new Office { Name = "D" };
                var ZZ = new Office { Name = "ZZ" };
                context.Offices.AddRange(new Office[] { A, B, C, D, ZZ });
                context.SaveChanges();
                #endregion
                #region Divisions
                var operation = new Division { Name = "OPERATION" };
                var Sales = new Division { Name = "SALES" };
                var MARKETING = new Division { Name = "MARKETING" };
                var CUSTOMER_CARE = new Division { Name = "CUSTOMER CARE" };

                context.Divisions.AddRange(new Division[] { operation, Sales, MARKETING, CUSTOMER_CARE });
                context.SaveChanges();
                #endregion
                #region Mike
                DateTime beginMike = new DateTime(2021, 05, 11);
                var MIkeSalary1 = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 10,
                    EmployeeId = Mike.Id,
                    DivisionId = operation.Id,
                    PositionId = Cargo_manager.Id,
                    OfficeId = C.Id,
                    Grade = 18,
                    BeginDate = beginMike,
                    BaseSalary = 2799.45m,
                    ProductionBonus = 0,
                    Compensation = 3215.63m,
                    Commission = 0,
                    Constributions = 1000
                };
                var MIkeSalary2 = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 9,
                    EmployeeId = Mike.Id,
                    DivisionId = operation.Id,
                    PositionId = Cargo_manager.Id,
                    OfficeId = C.Id,
                    Grade = 18,
                    BeginDate = beginMike,
                    BaseSalary = 2799.45m,
                    ProductionBonus = 0,
                    Compensation = 3263.99m,
                    Commission = 0,
                    Constributions = 1000
                };
                var MIkeSalary3 = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 8,
                    EmployeeId = Mike.Id,
                    DivisionId = operation.Id,
                    PositionId = HeadofCargo.Id,
                    OfficeId = D.Id,
                    Grade = 12,
                    BeginDate = beginMike,
                    BaseSalary = 2799.45m,
                    ProductionBonus = 0,
                    Compensation = 998.45m,
                    Commission = 0,
                    Constributions = 600
                };
                var MIkeSalary4 = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 7,
                    EmployeeId = Mike.Id,
                    DivisionId = operation.Id,
                    PositionId = Cargo_manager.Id,
                    OfficeId = C.Id,
                    Grade = 12,
                    BeginDate = beginMike,
                    BaseSalary = 2799.45m,
                    ProductionBonus = 0,
                    Compensation = 998.45m,
                    Commission = 0,
                    Constributions = 600
                };
                var MIkeSalary5 = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 6,
                    EmployeeId = Mike.Id,
                    DivisionId = operation.Id,
                    PositionId = cargoAssisten.Id,
                    OfficeId = D.Id,
                    Grade = 6,
                    BeginDate = beginMike,
                    BaseSalary = 2799.45m,
                    ProductionBonus = 2000,
                    Compensation = 1500,
                    Commission = 4500,
                    Constributions = 1450
                };
                var MIkeSalary6 = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 5,
                    EmployeeId = Mike.Id,
                    DivisionId = operation.Id,
                    PositionId = cargoAssisten.Id,
                    OfficeId = D.Id,
                    Grade = 6,
                    BeginDate = beginMike,
                    BaseSalary = 2799.45m,
                    ProductionBonus = 2000,
                    Compensation = 1500,
                    Commission = 4500,
                    Constributions = 1450
                };
                context.Salaries.AddRange(new EmployeeSalary[] { MIkeSalary1, MIkeSalary2, MIkeSalary3, MIkeSalary4, MIkeSalary5, MIkeSalary6 });
                context.SaveChanges();
                #endregion
                #region Kali
                DateTime beginkali = new DateTime(2021, 9, 9);
                var kali1 = new EmployeeSalary
                {
                    EmployeeId = Kali.Id,
                    Month = 10,
                    Year = 2021,
                    OfficeId = ZZ.Id,
                    PositionId = salesManager.Id,
                    DivisionId = Sales.Id,
                    Grade = 18,
                    BeginDate = beginkali,
                    BaseSalary = 2995,
                    ProductionBonus = 3000,
                    Compensation = 3200,
                    Commission = 0,
                    Constributions = 2100,
                };
                var kali2 = new EmployeeSalary
                {
                    EmployeeId = Kali.Id,
                    Month = 9,
                    Year = 2021,
                    PositionId = salesManager.Id,
                    DivisionId = Sales.Id,
                    OfficeId = ZZ.Id,
                    Grade = 18,
                    BeginDate = beginkali,
                    BaseSalary = 2995,
                    ProductionBonus = 3200,
                    Compensation = 3200,
                    Commission = 0,
                    Constributions = 2340,
                };
                context.Salaries.AddRange(new EmployeeSalary[] { kali1, kali2 });
                context.SaveChanges();
                #endregion
                #region Jane
                var jane1A = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 10,
                    Grade = 12,
                    EmployeeId = Jane.Id,
                    Constributions = 600,
                    DivisionId = Sales.Id,
                    PositionId = accountExecutive.Id,
                    OfficeId = ZZ.Id,
                    BeginDate = new DateTime(2021, 10, 10),
                    BaseSalary = 3000,
                    ProductionBonus = 4000,
                };
                var jane1B = new EmployeeSalary
                {
                    Year = 2021,
                    Month = 7,
                    Grade = 12,
                    EmployeeId = Jane.Id,
                    Constributions = 600,
                    DivisionId = Sales.Id,
                    PositionId = accountExecutive.Id,
                    OfficeId = ZZ.Id,
                    BeginDate = new DateTime(2021, 05, 02),
                    BaseSalary = 2799.45m,
                    ProductionBonus = 2365.74m
                };
                var jane2A = new EmployeeSalary
                {
                    Month = 6,
                    Year = 2021,
                    OfficeId = ZZ.Id,
                    EmployeeId = Jane.Id,
                    DivisionId = MARKETING.Id,
                    PositionId = MarketingAssitan.Id,
                    Grade = 11,
                    BeginDate = new DateTime(2021, 05, 02),
                    BaseSalary = 2799.45m,
                    ProductionBonus = 2666,
                    Compensation = 2365.74m,
                };
                var jane2B = new EmployeeSalary
                {
                    Month = 5,
                    Year = 2021,
                    OfficeId = ZZ.Id,
                    DivisionId = MARKETING.Id,
                    PositionId = MarketingAssitan.Id,
                    Grade = 11,
                    EmployeeId = Jane.Id,
                    BeginDate = new DateTime(2021, 05, 02),
                    BaseSalary = 2799.45m,
                    ProductionBonus = 2666,
                    Compensation = 2365.74m,
                };
                context.Salaries.AddRange(new EmployeeSalary[] { jane2A, jane1A, jane2B, jane1B });
                context.SaveChanges();
                #endregion
                #region ANN
                var ANN1A = new EmployeeSalary
                {
                    Month = 10,
                    Year = 2021,
                    EmployeeId = Ann.Id,
                    OfficeId = D.Id,
                    DivisionId = CUSTOMER_CARE.Id,
                    PositionId = customerDirector.Id,
                    Grade = 18,
                    BeginDate = new DateTime(2021, 05, 16),
                    BaseSalary = 5000,
                    Compensation = 3200,
                    Commission = 2478.96m,
                    Constributions = 1000
                };

                var ANN1B = new EmployeeSalary
                {
                    Month = 9,
                    Year = 2021,
                    OfficeId = D.Id,
                    DivisionId = CUSTOMER_CARE.Id,
                    PositionId = customerDirector.Id,
                    Grade = 18,
                    EmployeeId = Ann.Id,
                    BeginDate = new DateTime(2021, 05, 16),
                    BaseSalary = 5000,
                    Compensation = 3200,
                    Commission = 2677.88m,
                    Constributions = 1000
                };
                var ANN1C = new EmployeeSalary
                {
                    Month = 8,
                    Year = 2021,
                    OfficeId = D.Id,
                    DivisionId = CUSTOMER_CARE.Id,
                    PositionId = customerDirector.Id,
                    EmployeeId = Ann.Id,
                    Grade = 14,
                    BeginDate = new DateTime(2021, 05, 16),
                    BaseSalary = 3200,
                    Compensation = 998.45m,
                    Commission = 1845.66m,
                    Constributions = 600
                };
                var ANN1D = new EmployeeSalary
                {
                    Month = 7,
                    Year = 2021,
                    OfficeId = D.Id,
                    EmployeeId = Ann.Id,
                    DivisionId = CUSTOMER_CARE.Id,
                    PositionId = customerDirector.Id,
                    Grade = 14,
                    BeginDate = new DateTime(2021, 05, 16),
                    BaseSalary = 2799.45m,
                    Compensation = 998.45m,
                    Commission = 1944.22m,
                    Constributions = 600
                };

                var ANN1E = new EmployeeSalary
                {
                    Month = 6,
                    Year = 2021,
                    OfficeId = D.Id,
                    EmployeeId = Ann.Id,
                    DivisionId = CUSTOMER_CARE.Id,
                    PositionId = customerAssitan.Id,
                    Grade = 6,
                    BeginDate = new DateTime(2021, 05, 16),
                    BaseSalary = 2799.45m,
                    ProductionBonus = 1899,
                    Compensation = 2475.54m,
                    Commission = 896.22m,
                    Constributions = 0
                };
                var ANN1f = new EmployeeSalary
                {
                    Month = 5,
                    Year = 2021,
                    OfficeId = A.Id,
                    EmployeeId = Ann.Id,
                    DivisionId = CUSTOMER_CARE.Id,
                    PositionId = customerAssitan.Id,
                    Grade = 6,
                    BeginDate = new DateTime(2021, 05, 16),
                    BaseSalary = 2799.45m,
                    ProductionBonus = 1852.21m,
                    Compensation = 2475.54m,
                    Commission = 801.85m,
                    Constributions = 0
                };
                context.AddRange(new EmployeeSalary[] { ANN1A, ANN1B, ANN1C, ANN1D, ANN1E, ANN1f });
                context.SaveChanges();
                #endregion
            }




        }
    }
}
