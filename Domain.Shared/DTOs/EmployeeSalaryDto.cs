﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.DTOs
{
    public class EmployeeSalaryDto
    {

        //TODO: finish here
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeSurName { get; set; }
        public int OfficeId { get; set; }
        public string Office { get; set; }
        // pedding override data
        public int DivisionId { get; set; }
        public string Division { get; set; }
        public int PositionId { get; set; }
        public string Position { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal Compensation { get; set; }
        public decimal Commission { get; set; }
        public decimal Constributions { get; set; }
        public decimal TotalSalary { get; set; }
        public decimal OtherIncome { get; set; }
        public string FullName
        {
            get
            {
                return this.EmployeeName + " " + this.EmployeeSurName;
            }
        }
        public string IdentificacionNumber { get; set; }
        public DateTime BirthDay { get; set; }







    }
}
