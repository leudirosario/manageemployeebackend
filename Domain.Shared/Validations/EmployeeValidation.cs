﻿using Domain.Shared.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Validations
{
    public class EmployeeValidation : AbstractValidator<EmployeeDto>
    {
        public EmployeeValidation()
        {
            RuleFor(c => c.EmployeeName).NotEmpty().NotNull().WithMessage("Debe de espeficiar un nombre");
            RuleFor(c => c.EmployeeCode).NotEmpty().NotNull().WithMessage("Debe de espeficiar un codigo de empleado");
        }

    }
}
