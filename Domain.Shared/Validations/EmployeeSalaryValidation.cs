﻿using Domain.Shared.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Validations
{
    public class EmployeeSalaryValidation : AbstractValidator<EmployeeSalaryDto>
    {
        public EmployeeSalaryValidation()
        {
            // validation data
            RuleFor(c => c.BeginDate).NotNull().WithMessage("Debe digitar una fecha de inicio");
            RuleFor(c => c.Year).GreaterThan(0).NotNull().WithMessage("Debe digitar un año valido");
            RuleFor(c => c.Month).GreaterThan(0).LessThan(13).WithMessage("Debe digitar un mes valido");
            // must valid salary base 
            RuleFor(c => c.BaseSalary).GreaterThan(0).WithMessage("Debe de digitar un monto mayor a 0");
        }
    }
}
