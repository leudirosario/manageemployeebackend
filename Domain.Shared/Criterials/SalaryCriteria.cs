﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Shared.Criterials
{
    public class SalaryCriteria
    {
        public int OfficeId   { get; set; }
        public int Grade      { get; set; }
        public int PositionId  { get; set; }
        
    }
}
