﻿using Domain.Shared.Criterials;
using Domain.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IEmployeeService
    {
        public Task<IEnumerable<EmployeeDto>> GetEmployees(EmplyeeCriteria criteria);
        public Task<int> AddNewEmployee(EmployeeDto employeeDto);
    }
}
