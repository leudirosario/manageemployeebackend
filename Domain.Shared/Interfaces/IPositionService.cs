﻿using Domain.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IPositionService
    {
        public IEnumerable<PositionDto> GetAllPosition();
    }
}
