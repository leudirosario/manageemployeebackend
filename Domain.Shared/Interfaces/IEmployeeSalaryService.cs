﻿using Domain.Shared.Criterials;
using Domain.Shared.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Services.Interfaces
{
    public interface IEmployeeSalaryService
    {
        public Task<IEnumerable<EmployeeSalaryDto>> GetAllSalaries(SalaryCriteria salaryCriteria);
        public Task AddSalaries(List<EmployeeSalaryDto> Salaries);
    }
}
